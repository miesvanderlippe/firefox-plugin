console.log("Cajen is ready");

chrome.runtime.sendMessage({action: "screenshot_permission"}, function() {
    console.log("Asked background for screenshot permission");
});

chrome.runtime.onMessage.addListener(function(request, sender) {
    console.log(request);
    console.log(sender);

    switch(request.action)
    {
        case "screenshot_permission":
            if (request.data === true)
            {
                console.log("Creating screenshot");

                html2canvas(document.body).then(function(canvas) {
                    let b64 = canvas.toDataURL('image/jpeg', 1.0);
                    chrome.runtime.sendMessage({action: "screenshot_content", data: b64});
                })
            }
            else
            {
                console.log("No permission to create screenshot")
            }
    }
});