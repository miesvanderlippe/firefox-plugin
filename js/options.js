$(function() {

    // Obtain the port and host from local storage
    chrome.storage.local.get([
        "host",
        "port"
    ], function(resp) {
        $("#api_host").val(resp.host);
    });

    $("#save-button").click(function() {

        // Obtain all the form values
        let host = $("#api_host").val();
        let key = $("#api_key").val();
        let id = $("#api_id").val();
        let user_name = $("#user_name").val();
        let password = $("#password").val();

        // Create a CajenApi object using the obtained values
        let api = new CajenApi(
            user_name,
            password,
            id,
            key,
            host
        );

        // Get the bearer and expiration data from the API
        let bearer = api.getBearerToken();
        let expiration = api.getExpiration();
        let message = "Couldn't connect to Cajen";

        // If the we successfully obtained the bearer token we store all data
        if(bearer !== undefined && bearer.length > 0)
        {
            chrome.storage.local.set({
                bearer: bearer,
                expiration: expiration,
                host: api.host,
                port: api.port
            });

            // Change the message to display a successful message
            message = 'Connection Info Saved.';
        }

        // Obtain the status span and set its innerHTML accordingly
        let status = document.getElementById('status');
        status.textContent = message;

        // Remove the message after 2 seconds
        setTimeout(function() {
            status.textContent = '';
        }, 2000);
    });
});