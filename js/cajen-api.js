function CajenApi(user_name, password, client_id, client_secret, host, port=443)
{

    if (arguments.length === 3)
    {
        this.access_token = user_name;
        this.host = password;
        this.port = client_id;
    }
    else
    {
        this.host = host;
        this.port = port;

        this.url = "https://{0}:{1}/".format(this.host, this.port);

        let form_params = {
            'grant_type': 'password',
            'client_id': client_id,
            'client_secret': client_secret,
            'username': user_name,
            'password': password,
            'scope': '*'
        };

        let xhttp = new XMLHttpRequest();
        xhttp.open("POST", "{0}{1}".format(this.url, "oauth/token"), false);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify(form_params));

        let answer_text = xhttp.responseText;

        let answer = JSON.parse(answer_text);

        this.access_token = answer.access_token;
        this.expiration= answer.expires_in;
    }

    this.url = "https://{0}:{1}/".format(this.host, this.port);
}

CajenApi.prototype.getBearerToken = function()
{
    return this.access_token;
};

CajenApi.prototype.getExpiration = function()
{
    return this.expiration;
};

CajenApi.prototype.request = function(method, path, data, callback)
{
    let url = "{0}{1}".format(this.url, path);
    let xhttp = new XMLHttpRequest();
    let data_to_send = null;

    console.log("Sending a request to: " + url);

    xhttp.open(method, url);

    xhttp.onreadystatechange = function()
    {
        if (xhttp.readyState === 4 && xhttp.status === 200)
        {
            let js = JSON.parse(xhttp.responseText);

            if(typeof callback === "function")
            {
                callback(js);
            }
            else if (typeof data === 'function')
            {
                data(js);
            }
        }
    };

    if(arguments.length === 4)
    {
        data_to_send = JSON.stringify(data);
        xhttp.setRequestHeader("Content-type", "application/json");
    }

    xhttp.setRequestHeader("Authorization", "Bearer {0}".format(this.access_token));
    xhttp.setRequestHeader("Accept", "application/json");
    xhttp.send(data_to_send);
};

CajenApi.prototype.apiRequest = function(method, path, data, callback)
{
    let request_path = "api/{0}".format(path);

    this.request(method, request_path, data, callback);
};

CajenApi.prototype.getCases = function(callback)
{
    return this.apiRequest("GET", "case", callback);
};

CajenApi.prototype.getLogs = function(log_id, callback)
{
    this.apiRequest("GET", "case/{0}/log".format(log_id), callback);
};

CajenApi.prototype.saveEntry = function(case_id, log_id, description, specified_time, file_id, callback)
{
    let data = {
        description: description,
        specified_time: specified_time,
        file_id: file_id
    };

    this.apiRequest("POST", "case/{0}/log/{1}".format(case_id, log_id), data, callback);
};

CajenApi.prototype.uploadB64File = function(case_id, file_name, file, callback)
{
    let data = {
        file_name: file_name,
        file: file
    };

    this.apiRequest("POST", "case/{0}/file/base64".format(case_id), data, callback);
};