document.getElementById("activate-slider").addEventListener('click', function () {
    if(this.checked)
    {
        // When the slider is on we obtain the bearer host and port from the backend
        chrome.storage.local.get([
            "bearer",
            "host",
            "port"
        ], function(result) {

            // We create a new CajenApi object using the obtained values
            let cajen = new CajenApi(
                result.bearer,
                result.host,
                result.port
            );

            // Obtain all cases from the Cajen API
            cajen.getCases(function (allCases) {

                // Generate a table based on the retrieved cases
                let caseHtml = "<table class='table delete-on-update'>" +
                    "<thead>" +
                    "<tr>" +
                    "<th>ID</th><th>Name</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody class='table-highlight-tr-hover' id='table-case'>";

                for(let i = allCases.length - 1; i >= 0; i--)
                {
                    caseHtml += "<tr data-case-id='" +
                        allCases[i].id +
                        "'>" +
                        "<td>" +
                        allCases[i].id +
                        "</td>" +
                        "<td>" +
                        allCases[i].name +
                        "</td>" +
                        "</tr>";
                }

                caseHtml += "</tbody></table>";

                $('#activate-content').html(caseHtml);
            });

        });
    }
    else
    {
        // If the slider is turned off we turn activation off and reset all the
        // ID references
        chrome.storage.local.set({
            activated: false,
            case_id: null,
            log_id: null,
            log_name: null,
        });

        $('#activate-content').html("");
    }
});


$(function() {

    // If the popup is opened we check whether the plugin is activated based on the values from local Sstorage
    chrome.storage.local.get([
        "activated",
        "log_id",
        "log_name"
    ], function(result) {

        // If the plugin is activated we change the standard HTML to the activated HTML
        if(result.activated !== undefined && result.activated === true)
        {
            setActivatedHtml(result.log_id, result.log_name);

            $('#activate-slider').prop('checked', true);
        }
    });

    // Add a listener to the body to check for row clicks on the case table
    $("body").on("click", "#table-case tr", function(event) {

        // Retrieve the case id from the clicked row
        let caseId = $(this).attr("data-case-id");

        // Retrieve data from local storage to build a CajenApi object
        chrome.storage.local.get([
            "bearer",
            "host",
            "port"
        ], function(result) {

            // Create a CajenApi object
            let cajen = new CajenApi(
                result.bearer,
                result.host,
                result.port
            );

            // Pull all the logs for the current case from Cajen
            cajen.getLogs(caseId, function(allLogs) {

                // Remove the current table from the popup
                $(".delete-on-update").remove();

                // Set our current case id
                chrome.storage.local.set({
                    case_id: caseId
                }, function() {

                    // Generate a table (like we did with the cases) based on the retrieved logs
                    let logHtml = "<table class='table'>" +
                        "<thead>" +
                        "<tr>" +
                        "<th>ID</th><th>Name</th>" +
                        "</tr>" +
                        "</thead>" +
                        "<tbody class='table-highlight-tr-hover' id='table-log'>";

                    for(let i = allLogs.length - 1; i >= 0; i--)
                    {
                        logHtml += "<tr data-log-id='" +
                            allLogs[i].id +
                            "' data-log-name='" +
                            allLogs[i].name +
                            "'>" +
                            "<td>" +
                            allLogs[i].id +
                            "</td>" +
                            "<td>" +
                            allLogs[i].name +
                            "</td>" +
                            "</tr>";
                    }

                    logHtml += "</tbody></table>";

                    $('#activate-content').html(logHtml);
                });

            });

        });
    }).on("click", "#table-log tr", function() {
        // Add a listener for row clicks on the log table and obtain the ID and name from the clicked row
        let logId = $(this).attr("data-log-id");
        let logName = $(this).attr("data-log-name");

        // Change the HTML to the activated HTML
        setActivatedHtml(logId, logName);

        // Set the activated boolean to true as we're now logging all browser activity
        chrome.storage.local.set({
            activated: true,
            log_id: logId,
            log_name: logName
        });
    });
});

let setActivatedHtml = function(log_id, log_name)
{
    $('#activate-content').html(
        "<h2>Now logging to " +
        log_id +
        " - " +
        log_name +
        "</h2>");
};
